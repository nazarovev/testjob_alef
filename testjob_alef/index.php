<?php require 'header.php'; ?>

<div class="starter-template text-center py-5 px-3">
    <h1>Тестовые задания для Alef Development</h1>
    <p class="lead">Исполнитель: Евгений Назаров <br> Резюме: <a href="http://resume.nzrv.su/" target="_blank">resume.nzrv.su</a></p>
    <br><br>
    <a class="btn btn-lg btn-success mr-2" href="/testjob_alef/job_1.php">Задание 1</a>
    <a class="btn btn-lg btn-success mr-2" href="/testjob_alef/job_2.php">Задание 2</a>
    <a class="btn btn-lg btn-success" href="/testjob_alef/job_3.php">Задание 3</a>
</div>

<?php require 'footer.php'; ?>