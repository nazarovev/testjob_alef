<?php

/** Если есть параметр GO в строке GET запроса - то выполняем решение */
if (isset($_GET['go'])) {
    /** Решение задачи */

    /** Загаданное число */
    $number = '3810';
    $number_split = str_split($number);

    /** Массив чисел, предлагаемых игроком */
    $arr_numbers = [
        '2679',
        '1234',
        '5678',
        '0183',
        '3801',
        '3810'
    ];

    $arr_result = [];

    /** Тут я перебираю массив предлагаемых игроком чисел */
    foreach ($arr_numbers as $item) {
        /** Разбиваю строку с числом на символы */
        $item_split = str_split($item);
        /** Ввожу переменную полных совпадений */
        $matches = 0;
        /** В этом цикле считаю полные совпадения */
        for ($i = 0; $i<4; $i++) {
            if ($item_split[$i] == $number_split[$i]) {
                $matches++;
            }
        }

        /** Ввожу переменную угаданных чисел */
        $exists = 0;
        /** В этом цикле считаю угаданные числа */
        foreach ($item_split as $item_char) {
            foreach ($number_split as $number_char) {
                if ($item_char == $number_char) {
                    $exists++;
                    break 1;
                }
            }
        }

        /** Проверяю совпадения, если все полностью совпали, помечаю победителем */
        $win = 0;
        if ($matches == 4) {
            $win = 1;
        }

        /** Пишу результаты в один массив для удобного вывода на странице */
        $arr_result[] = [
            'number' => $item,
            'matches' => $matches,
            'exists' => $exists,
            'win' => $win,
        ];
    }
}


?>


<?php require 'header.php'; ?>

    <div class="starter-template text-center py-5 px-3">
        <h1>Задание 2</h1>
        <p>Задание на базе игры "Быки и коровы". Суть игры состоит в том, что ведущий загадывает четырехзначное число, а
            игрок пытается его угадать. Игрок на каждом ходу сообщает ведущему свое предположение (четырехзначное число,
            тот в свою очередь овтечает ему: "ты угадал столько-то букв, из них столько-то на своем месте").</p>
        <p>Например: если ведущий загадал число 3810, а игрок предположил "0856", то ведущий должен ответить "угадал 2
            цифры, из них на своем месте 1 цифра". Итак задание: ваша программа является ведущим в этой игры. Загадно
            число "3810". Программа должна последовательно вывести ответы на следующие числа: 2679, 1234, 5678, 0183,
            3801, 3810. Каждый ответ должен быть на новой строке и должен быть записан в формате "x-x", где первое число
            это количество совпавших цифр, а второе — кол-во совпавших цифр, находящихся на своей позиции.</p>

    </div>
    <div class="starter-template text-center">
        <a href="?go" class="btn btn-info btn-lg">ВЫПОЛНИТЬ</a>

    </div>

<?php if (isset($_GET['go'])): ?>
    <div class="starter-template text-center py-5 px-3">
        <h2 class="pb-4">Результат</h2>
        <p>
            <strong>Загадано число:</strong>
            <br>
            <strong style="color: darkgreen"><?php echo $number; ?></strong>
        </p>
        <p>
            <strong>По очереди перебираем числа, предлагаемые игроком:</strong>

            <?php foreach ($arr_result as $item_result): ?>
            <p <?php if($item_result['win'] === 1) { echo 'style="color: red;"'; } ?>>
                Число игрока: <strong><?php echo $item_result['number']; ?></strong>.
                Угадал <strong><?php echo $item_result['exists']; ?></strong> цифр, из них <strong><?php echo $item_result['matches']; ?></strong> на своем месте.
                <?php if($item_result['win'] === 1): ?>
                    <span style="color: red;" class="text-bold">ЭТО ПОБЕДА</span>
                <?php endif; ?>
            </p>
            <?php endforeach; ?>

        </p>

    </div>
<?php endif; ?>

<?php require 'footer.php'; ?>