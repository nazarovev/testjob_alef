<?php

/** Если есть параметр GO в строке GET запроса - то выполняем решение */
if (isset($_GET['go'])) {
    /** Решение задачи */

    /** Определяем массив, где сохраним подходящие числа для вывода на экран */
    $arr_result = [];
    /** Определим переменную для накопления суммы */
    $summ = 0;
    /** Перебираем числа от 1 до 1000 включительно */
    for($i = 1; $i <= 1000; $i++) {
        /** Преобразуем число в строку */
        $str = (string) $i;
        /** Разбиваем строку в массив символов */
        $str_split = str_split($str);
        /** Циклом проходимся по каждому символу, ищем одинаковые */
        $flag_wrong = 0;

        foreach ($str_split as $key_1 => $char_1) {
            foreach ($str_split as $key_2 => $char_2) {
                if ($key_1 != $key_2) {
                    if ($char_1 == $char_2) {
                        $flag_wrong = 1;
                        break 2;
                    }
                }

            }
        }

        if ($flag_wrong == 0) {
            /** Этот код выполняется, если одинаковые числа не переключили флаг */
            /** Добавляем в массив число, подходящее по условию */
            $arr_result[] = $i;
            /** Добавляем подходящее число к сумме */
            $summ += $i;
        }

    }

}


?>


<?php require 'header.php'; ?>

    <div class="starter-template text-center py-5 px-3">
        <h1>Задание 3</h1>
        <p>Возьмите все числа от 1 до 1000 (включительно). Выбросьте из этой последовательности все числа, где одна и та же цифра встречается более, чем 1 раз. Найдите сумму оставшихся чисел.</p>
    </div>
    <div class="starter-template text-center">
        <a href="?go" class="btn btn-info btn-lg">ВЫПОЛНИТЬ</a>
    </div>

<?php if (isset($_GET['go'])): ?>
    <div class="starter-template text-center py-5 px-3">
        <h2 class="pb-4">Результат</h2>
        <p>
            <strong>Список подходящих по условию чисел:</strong>
            <br>
            <span style="font-size: x-small"><?php echo implode(', ', $arr_result); ?></span>
        </p>
        <p>
            <strong>Сумма подходящих по условию чисел:</strong>
            <br>
            <strong style="color: darkgreen;"><?php echo $summ; ?></strong>
        </p>

    </div>
<?php endif; ?>

<?php require 'footer.php'; ?>